const flattenFunction = require('./flatten.cjs');

const nestedArray = [1, [2], [[3]], [[[4]]]];

console.log(flattenFunction(nestedArray,2));