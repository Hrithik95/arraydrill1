function each(arr, cb) {
    if (!Array.isArray(arr) || arr.length == 0) {
        return [];
    }
    for (let index = 0; index < arr.length; index++) {
        arr[index] = cb(arr, index);
    }
    return arr;
}

module.exports = each;