function find(arr, cb){
    if(!Array.isArray(arr) || arr.length == 0 ){
        return undefined;
    }
    for(let index=0;index<arr.length;index++){
        let value = cb(arr[index]);
        if(value){
            return value;
        }
    }
    return undefined;
}

module.exports = find;