const eachFunction = require('./each.cjs');

const items = [1, 2, 3, 4, 5, 5];

function square(array, index) {
    return array[index] * array[index];
}

console.log(eachFunction(items,square));