const filterFunction = require('./filter.cjs');

const items = [1, 2, 3, 4, 5, 5];

function isEven(value,index,items){
    if(value % 2 === 0){
        return true;
    }
}

console.log(filterFunction(items,isEven));