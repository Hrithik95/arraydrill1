
const getFlattenArray = (elements, depth) => {
    if (!Array.isArray(elements)) {
        return [];
    }
    if (depth === undefined) {
        depth = 1;
    }
    let newArray = [];
    for (let index = 0; index < elements.length; index++) {
        if (Array.isArray(elements[index]) && depth > 0) {
            newArray = newArray.concat(getFlattenArray(elements[index], depth - 1));
        } else if (elements[index] !== undefined) {
            newArray.push(elements[index]);
        }
    }
    return newArray;
}

module.exports = getFlattenArray;

