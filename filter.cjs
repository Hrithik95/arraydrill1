function filter(arr, cb) {
    if (!Array.isArray(arr) || arr.length === 0 || typeof cb !== "function") {
        return [];
    }
    let result = [];
    for (let index = 0; index < arr.length; index++) {
        let value = cb(arr[index], index, arr);
        if (value === true) {
            result.push(arr[index]);
        }
    }
    return result;
}

module.exports = filter;