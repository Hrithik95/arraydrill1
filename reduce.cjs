function reduce(arr, cb, startingValue) {
    if (!Array.isArray(arr) || arr.length == 0 || typeof cb != "function") {
        return;
    }
    let acc;
    let index = 0;
    if (startingValue == undefined) {
        acc = arr[0];
        index++;
    } else {
        acc = startingValue;
    }
    for (; index < arr.length; index++) {
        let value = cb(acc, arr[index], index, arr);
        acc = value;
    }
    return acc;
}

module.exports = reduce;

