function map(arr, cb) {
    if (!Array.isArray(arr) || arr.length == 0 || typeof cb != "function") {
        return [];
    }
    const result = [];
    for (let index = 0; index < arr.length; index++) {
        let element = cb(arr[index],index,arr);
        result.push(element);
    }
    return result;

}

module.exports = map;